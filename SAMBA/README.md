[TOC]


# SAMBA Installation konfigurieren Ubuntu 22.04 


## Einleitung

In dieser Anleitung konfigurieren wir Sambashare zu einem Windows Client.
Du brauchst einen Ubuntu mit Desktop version 22.04. 

## Konfiguration


### 1. Gebe den folgenden befhel aus:

Dieser Schritt dient dazu, denn System aktualisieren und um sicher zu stellen ob das System vollständig konfiguriert ist. 
Führe folgendes im Terminal aus.

---
![Alt text](./images/image.png) 

## Samba Installation

Dieser befehl dient dazu um SAMBA zu installieren 

### 2. Gebe folenden befehl ein:

---
![Alt text](./images/image-1.png)

### 3. Gebe folgenden befhel ein:

Mit diesem befehl kannst du überprüfen, ob de installation von SAMBA funktioniert hat.

---
![Alt text](./images/image-2.png)

Es sollte so aussehen: 


![Alt text](./images/Screenshot%202024-01-17%20092300.png)

## Benutzer hinzufügen.

### 4.  Gebe folgenden befehl ein:

Füge dein Benutzer zu Sambashare hinzu.

---
![Alt text](./images/image-3.png) 

**$USER** = Deinen eigenen Benutzer die du bei deinem Ubuntu eingerichtet hast. 
in meinem fall währe es **Sofia**

### 5. Zum anschluss gibst du diesen befehl aus:

Dies dient dazu um ein Passwort festzustellen.

----
![Alt text](./images/image-4.png)

## Teile dein Ubuntu Ordner

### 6. Erstelle einen Ordner in deinem Home verzeichnis. 

Gehe auf **Properties** des Ordners. ANschliessend auf **Local Network Share** und aktivierst **Share this folder** und **Allow others to creat and delete files in this folder. 

---

![Alt text](./images/Screenshot%202024-01-17%20100639.png)


### 7. Folder Share
Gehe auf **Local Network Share** wähle **Share this folder** und **Allow others to create and delet files in this folder** und anschliessend **Creat Share**.

---

![Alt text](./images/Screenshot%202024-01-15%20112816.png)

Akzeptiere die **Add the permissions automatically** 


### 8. Regristieren des Servers
Gehe auf **Other Locations** und gebe bei **Connect to Server** und gebe folgendes ein **smb://ip-adresse von ubuntu/shared- folder -name**

---

![Alt text](./images/Screenshot%202024-01-15%20113432.png)


### 9. Setze ein Passwort 
wähle **Registered User** aus 

---

![Alt text](./images/Screenshot%202024-01-17%20095405.png) 

## Windows zugriff 

Gehe auf Windows in den Explorer. 
Auf dem suchfeld gibst du **\\\ip adresse von ubuntu\shared folder**

Anschliessend kommt ein Anmelde Fenster. Gebe den Nutzername deines Ubuntu ein und das Passwort das du gesetzt hast ein. 

---
![Alt text](./images/Screenshot%202024-01-22%20112804.png)

### Zugriff 

Wenn die Konfigurierung geklappt hat solltest du auf deinem Erstellten Ordner zugriff haben.

---
![Alt text](./images/Screenshot%202024-01-22%20112814.png) 