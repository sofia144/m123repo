[TOC]

## Einleitung
Dies ist eine Kurze Dokumentation zu dem Auftrag "Netzwerk für ein KMU einrichten".

### Start 

Lade die Filius Datei herunter und öffne es. 

1. Verbinde die Geräte und die IP-Ranges wie auch der Websitename vom Webserver aufgenommen.

## SW01 Netzwerk DHCP & 6 CLients 

Ich hab die CLients mit dem DHCP Server eingerichtet. 
So sollten die Enstellungen aussehen.!

![Alt text](img3.png)

2. Gebe die vorgegebene Netzwerkmaske ein.

3. Um sicherzustellen, dass einer der sechs Clients eine statische IP-Adresse hat, habe ich dem Client PC11 mithilfe der MAC-Adresse eine statische IP-Adresse über den DHCP-Server zugewiesen.

so sollte es aussehen: 

![Alt text](img5.png)

Danach aktiviere bei dem Client **DHCP zur Konfiguration verwenden**

## SW03 Netzwerk 2 Clients mit Statischer IP 

Im nächsten Schritt habe ich die Konfiguration für die beiden Clients am SW03-Switch vorgenommen.

## SW02 Netzwerk WEb & DNS Server

Zuerst habe ich den Servern die IP Einstellungen eingerichtet.

DNS Server:

![Alt text](img9.png)

Webserver:

![Alt text](img10.png)

### Ping Test

Ich führe diese Konfiguration von einem DHCP-Client aus, um gleichzeitig zu überprüfen, ob der DHCP reibungslos funktioniert.

![Alt text](img11.png)

Wenn du in jedem Netzwerk ein Gerät pingen kannst sollte alles in Ordnung sein. 

### Web & DNS Server einrichten

Zuerst starten wir den Webserver. Da wir dies in Filius durchführen, ist es nicht erforderlich, eine Website zu erstellen. Stattdessen fügen wir lediglich den Dienst hinzu und starten ihn.

So geht es: 

1. Webserver bei "Software-Installationen" installieren und öffnen.

![Alt text](img12.png)

2. Klicke **Start**
![Alt text](img13.png)

Nun müssen wir den DNS konfigurieren. Ich habe für meine Website die Webadresse "splendid-blur.ch" mit der IP-Adresse 172.16.100.4 verwendet.

Füge Ausserdem PC11, PC51 & PC52 Hinzu.

![Alt text](img14.png)

### Web & DNS Server Testen 

Um die beiden Server zu testen, müssen wir nun auf einen Client zugreifen. Da ich alle Clients ordnungsgemäß konfiguriert habe, sollte jeder Client für die Tests funktionieren.

Ein mögliches beispiel wie es aussehen könnte:

![Alt text](filiustest.png)