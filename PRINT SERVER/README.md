[TOC]

### Einleitung 

In dieser Dokumentation geht es um einen Print Server einzurichten auf Windows Server.

- Du brauchst ein Windows Server und einen Windows Client. 

## Printserver einrichten

Auf deinem Windows Server unter **Tools** gehst du auf **Print Managment**.

Gehe auf **Printer** und mache ein rechtsklikc in das lehre und klicke **add Printer**
Trage dan die beliebige Adresse deines Druckers ein und Stelle um auf **TCP/IP Device** 

![Alt text](<Screenshot 2024-01-22 162459.png>)

Als nächstest wählst du den richtigen Trieber aus. 
Diesen kannst du auf der HP Website herunterladen. 

![Alt text](<Screenshot 2024-01-29 111629.png>)

NAch diesem schritt sollte der Drucker erfolgreich erstellt sein. 

![Alt text](<Screenshot 2024-01-29 105047.png>)

Drucke eine Testseite aus. 
Dies kannst du mit rechtsklick auf den Drucker machen. 

## Auf dem Client drucken 

Gehe auf deinem Client und versuche in CMD mit dem Befehl **Ping** auf deinem Server zugreifen und anschliessend noch deinen Drucker Pingen. 

![Alt text](<Screenshot 2024-01-29 105321.png>)

Öfnne im Explorer deines Clients dein Server Netzwerk -> \\172.16.17.45
und gebe deine Anmeldedaten ein und klicke auf **Anmelde Daten Speichern** und so solltest du von deinem CLient drucken können. 