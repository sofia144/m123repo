[TOC]

# DNS Configoration on Windows Server

## Einleitung 

In dieser Dokumentation geht es darum einen DNS-Server auf Windows Server 2022 zu installieren.

Ich habe meine Konfiguration anhand einer Dokumentation eines Klassenmitglieds durchgeführt. 

## DNS Installieren 

- Du brauchst einen Virtuelen Windows Server 2022 

## Statische IP-Adresse Vergeben

Du musst  deinem Server eine Statische IP-Adresse vergeben. 
Das kannst du in den Einstellungen bei **Change adapter option** ändern. 

stellen sie den DNS Bereich wie in diesem Bild ein:

![Ip adresse](./images/11.png)

Die IP-Adresse wird bei dir wahrscheinlich anderst sein.

- 127.0.0.1 localhost also lokale DNS
- 8.8.8.8 für den Google DNS 

Speichere die Einstellungen und mache ein restart. 

## DNS auf Windows Server Installieren

Öffne das Dashboard im Servermanager, dies ist eventuell schon beim start geöffnet. 

Klicke im Dashboard auf **2 Add roles and features**. 

Anschliessend folge die Bildanleitung.

1. Klicke auf **Add roles and features**
---
![addroles](./images/12.png)

2. Skippe bis du zu **Select server roles**. Dann wählst du **DNS** an. Klicke auf **Install**. 
---

![selectdns](./images/13.png)

Nach der Installation kannst du das Fenster schliessen. 

## DNS Konfigurieren 

### Forward Lookup Zone Einrichten

1. Öffne die DNS Konfiguration, um die Lookupzones zu Konfigurationen. Info: Dies muss gemacht werden damit der DNS überhaupt im Netzwerk Sichtbar ist. 
---
![dns](./images/14.png)

2. Rechtsklick auf **Forward Lookup Zone** und **New Zone** anklicken. 
---
![newzone](./images/15.png)

3. Wähle eine **Primary Zone** aus.
---
![primaryzone](./images/16.png)

4. Bennene deine DNS Zone mit deiner Domain Name. 

Domain Name kannst du beliebig nennen. 

---

![name](./images/17.png)

5. Erlaube *keine* Dynamische updates.
---

![5](./images/18.png)

Jetzt kannst du die Konfiguration abschliessen. 

## Reverse Lookup Zone Einrichten

1. Rechtsklick auf **Reverse Lookup Zone** und dan auf **New Zone**. 
---

![](./images/19.png)


2. Mache erneut eine **Primary Zone**
---

![](./images/20.png)


3. Wir erstellen eine IPv4 Lookup Zone. 
---

![](./images/21.png)

4. Gebe deine Netzwerk ID ein.
---
![](./images/22.png)


5. Erlaube *keine* Dynamische Updates. 
---
![](./images/23.png)

Die Konfiguration kann abgeschlossen werden. 

## Finale Konfiguration 

### Firewall Deaktivieren 

Deaktiviere noch die Firewall. Dies gilt zur sauberen Kommunikation zwischen deinem Client und Server.

1. Gehe dafür bei deinem Client und Server in ihre Firewall Einstellungen. Gebe dafür einfach in der Suchleiste **Defender** ein und klicken sie auf **Windows Defender Firewall**. 

---

![firewll](./images/24.png)

2. Klicke jetzt auf **Inbound Rules** und suche dann die **File and Printer Sharing (Echo Request - ICMPv4-In)**.
Mache darauf einen Rechtsklick und klicken sie dann auf Properties. 

---

![25](./images/25.png)

3. Wähle bei den Properties das "Advanced" Tab aus. Beim Profile bei allen Kästchen einen hacken setzen. 
---
![hacken](./images/26.png)

4. Aktivieren dan ihre neue Regel. 
---
![aktivieren](./images/27.png)
---

## Host und Mail Exchanger Hinzufügen

### Host Hinzufügen

Ohne den Host würde der DNS nicht im Netzwerk erreichbar sein. 

1. Mit Rechtsklick auf **Forward Lookup Zone** die du erstellt hast und wähle **New Host** aus.
---
![newhost](./images/28.png)

2. Gebe hier einen beliebigen Namen und eine IP-Adresse ein. Zusätzlich musst du einen Hacken beim Kästchen für den **PTR** setzen. 
---
![web](./images/29.png)

Füge einen Host für den Client ein. Gehe dafür nochmals mit Rechtsklivk auf **New Host**.

### Mail Exchanger Hinzufügen 

1. Rechtsklick auf **Forward Lookup zone** die du erstellt hast. Wähle jetzt abr **New Mail Exchanger** aus. 
---
![mailexchanger](./images/28.png)

2. Gebe dein Hostdomain ein und wähle den Host den du erstellt hast als **FQDN** aus. 
---
![mail](./images/30.png)

## Überprüfung der Reverse Lookup Zone 

1. Gehe dafür auf die Reverse Lookup Zones und Überprüfe, ob beide Hosts einen Pointer Hinterlegt haben. 
---
![records](./images/31.png)

Falls die Pointer nicht hinterlegt sind, kannst du die Pointer einfach hinzufügen indem du ein Rechtsklick auf deine erstellte **Reverse Lookup Zone** und klicke dann auf **New Pointer** und gebe die benötigten Daten ein. 


---

![pointer](./images/32.png)


## Zwischen Troubleshooting 

Be meinem Windows Server hatte ich keine Netzwerk verbindung, weil ich bei meiner 2. Netzwerkkarte den DHCP nicht aktiviert hatte und somit erhielt ich eine APIPA Adresse und konnte nicht ins Internet raus. 

---

![trouble](./images/33.png)

Dein DNS Server sollte jetzt eine Internetverbindung haben. 

## DNS Konfiguration auf den Client 

1. Öffne die Netzwerkeinstellungen und ändere vom NAT Netzwerkadapter die DNS Einstellungen.
Deine DNS Adresse hängt von der IP-Adresse deines Windows Server ab.
 ---
 ![NAT](./images/34.png)

## Forwarders Konfigurieren 

1. Mit Rechtsklick auf deinem Server **WIN-..........**
2. Anschliessend klickst du auf **Properties**
---
![server](./images/36.png)

3. Gehe ind **Forwarders** Tab und dann auf **Edit**.

Das dient dazu die Anfrragen die dein DNS Server nicht beantworten kann. Es auf einen anderen Server senden kann. 

In habe dazu Google DNS benutzt -> 8.8.8.8
---
![googledns](./images/40.png)

## Teste dein DNS 

Zum schluss müssen wir den DNS Server Testen um zu prüfen, ob die Konfiguration erfolgreich war. 

1. Öffne bei deinem Windows Server Powershell un gebe folgende Commands eingeben werden:

- nslookup [Windows Server IP] 
- nslookup [Client IP Adresse]
- Ping     [Client IP Adresse]
---
![snlookup](./images/37.png) 
![ping](./images/38.png)

## DNS Client Testen 

Starte dein Client und öffne dafür **Powershell** 

1. Gebe folgendende Commands ein:

- nslookup [Server IP Adresse]
- nslookup [Mail Excanger]
- nslookup [Client]
---
![clienttest](./images/39.png)

Wenn sich dein Client und DNS Server Pingen können hat die Konfiguration Erfolgreich geklappt. 
---