[TOC]


# 1. Einführung & Erklärung 

## 1.1 Einführung 

Die Ausgangslagge ist ein DHCP Server auf Ubuntu-Server von Terminal aus zu installieren und ein Windows Client einriechten. 


## 1.2 DHCP

Ein DHCP ist dafür, da um automatisch und kronket zu vereinfachen. Bei einer Anzahl von 10 Benutzern ist die Verwendung eines DHCP-Servers sinnvoll und empfohlen. Die Verbindungsschritte und Kommunikation zwischen DHCP-Server und Client sind im DORA-Prozess beschrieben. Es gibt 4 wichtige Schritte: DHCPDiscover – Genaues Suchen der einzelnen Netzwerkkomponenten per Broadcast.
DHCPOffer – DHCP erhält per Port 67 eine Anfrage zur Vergabe und Reservierung einer IP-Adresse und schickt die einzelnen Daten (Lease-Time, IP, Gateway, DNS) zur Verbindung an dem Client zurück (via Broadcast).
**DHCPRequest** – Der Client akzeptiert die Offerte und meldet es per Broadcast am **DHCP-Server.**
**DHCPAck(nowledge)** – Der DHCP bekommt die akzeptierte Offerte zurück und fängt mit der Reservierung der IP-Adresse an, vielleicht auch schon die direkte Vergabe der IP-Adresse.


## 2. Übersicht 

![Übersicht](./images/images/8.png)

Mein DHCP Server ist mit der mit der IP Adresse 192.168.182.10 eingerichtet. 

## 3. Netzwerkapadpter 

![Netzwerkkarte](./images/images/9.png)

Bei der Netzwerk Karte muss **NAT** eingestellt sein. 

**DHCP** musst du auf **Connect a host virtual adapter to this network**. 

## 4. Installation Ubuntu Server 

## 4.1 Ubuntu Installieren 

**1.** Das ist unser Startbildschirm, hier habe ich "try or install ubuntu server" ausgewählt. 

![Ianstall](./images/images/10.png)

**2.** Die folgenden Schritte nach dem Wählen der Option sind einfach nachzuvollziehen. Man soll nur durchklicken und Einstellungen nach Belieben auswählen. 

**3.** Ich habe beim Installieren direkt die statische IP Adresse eingerichtet.

Subnet:         192.168.182.0/24

Address:        192.168.182.10

Gateaway:       102.168.182.2

Name servers:   8.8.8.8 


**4.** Das eingeben der Namen ist dir überlassen:


## 4.2 Installation von DHCP Server 

**1.** mit dem folgendem Befehl laden wir unser DHCP-Dienst herunter.


**sudo apt install isc-dhcp-server**

**2.** Nach einer Weile soll der DHCP verfügbar auf dem System heruntergeladen sein.

**3.**Wir wollen jetzt also den DHCP-Server konfigurieren. Zuerst mal ein Backup der Config-Datei. Dies macht man mit:

**sudo cp /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.backup**

**4.**  Mit **sudo nano /etc/dhcp/dhcpd.conf** öffnet man diese zuvor redundant gespeicherte Datei. Das Bild unten ist die geöffnete Konigurationsdatei, wo wir unsere IP-Adressen, Pools und sonstiges konfigurieren.

![dhcpd.conf](./images/images/11.png)

**5.** Wir müssen noch folgende Adressen an geben. 

Subnetz-IP      192.168.182.0 
Subnetzmaske    255.255.255.255.0
IP Pool range   192.168.182.100 - 192.168.182.151
DNS Server      8.8.8.8 
Gateaway        192.168.182.2 
Bei mir ist der Gateway 2, weil VMware was so möchte.

Broadcast Adresse und Leasetime in Minuten werden ebenfalls eingegeben. Schon ist unsere Konfiguration für den DHCP Server teils fertig. 

![Config](./images/images/12.png)

**6.** Bei INTERFACEv4 soll anstatt eth'0 -> ens37 stehen. Wir geben vor, dass ens37 das Netzwerkapter für die DHCP- Requests ist. 

![eth 0](./images/images/13.png)


## 5. Client Konfiguration 

Gehe auf dein Windows Client den Netzwerkadapter anpassen. 

![Ethernet](./images/images/14.png)



Dannach auf **Eigenschaften** und schaue nach - ![vpn4](./images/images/15.png) und gehe dort auf **Eigenschaften**.



Jetzt musst du **Obtain an IP address automatically** auswählen.
![IPauto](./images/images/16.png)


Öffne jetzt **CMD** auf deinem Windows Client und gebe folgende befhele ein. 

**ipconfig /release** = um die aktuelle IP-Adresse freizugeben

**ipconfig /renew** = um eine neue zu holen. 

Der Client soll vom DHCP Server eine IP Adresse in der IP Range bekommen. 



