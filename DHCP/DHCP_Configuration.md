[TOC]

# DHCP Configuration
In dieser Dokumentation wird erklärt, wie man auf Filius einen **DHCP-Server** aufsetzen und konfiguriert. 

## DHCP Setup 
Bei der Ausgangslage handelt es sich um eine
**Sterne-Architektur** mit einem **DHCP-Server** und **drei CLients** 

![Architektur](./images/1.png)

## Konfigurationsschritte
-Statische IP Adressenzuweisung

-IP Range einrichten 

-Überprüfung mit "Ping" 


## Statsiche IP Adressenzuweisung  

![DHCP-Server](./images/4.png)

DU musst beim gewünschten Client 3 **"DHCP Server einrichten"** auswählen und dann auf **"Statische Adressenzuweisung"** . Gebe die MAC Adresse des CLients ein und die gwünschte IP Adresse in diesem fall **192.168.0.50** und anschliessend auf hinzufügen. 


## IP Range einrichten

![IP Range](./images/5.png)

Beim **DHCP-Server** muss du die gewünschten **Unter- und Obergrenze Adressen** einrichten. 

**192.168.0.150 - 192.168.0.170** 

Vergesse nicht **DHCP aktivieren**. 


![DHCPaktivieren](./images/6.png)
Nun musst du bei allen Clients **DHCP zu Konfiguration verweden** aktivieren.

## Überprüfung der Konfiguration

![ping](./images/7.png)
Du kannst von Client 1 zu CLient 3 einen Ping machen, wenn du eine Antwort erhälst dann hat es geklappt. 





## 1. Router-Konfiguration auslesen 

**Für welches Subnetz ist der DHCP Server aktiv?**

Subnetzmaske


**Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort als Tabelle)**

192.168.27.29    0009.7CA6.4CE3   
192.168.27.32    00E0.8F4E.65AA           
192.168.27.31    0050.0F4E.1D82          
192.168.27.33    0007.ECB6.4534           
192.168.27.30    0001.632C.3508           
192.168.27.34    00D0.BC52.B29B    

**In welchem Range vergibt der DHCP-Server IPv4 Adressen?**

192.168.27.1 - 192.168.27.254

**Was hat die Konfiguration ip dhcp excluded-address zur Folge?**

Das man die excluded adresse nicht mehr vergeben kann.


**Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?**

252

## 2. DORA - DHCP lease beobachten 

**Welcher OP-Code hat der DHCP-Offer?**

 Op Code (op) = 2 (0x2)

**Welcher OP-Code hat der DHCP-Request?**

Op Code (op) = 1 (0x1)

**Welcher OP-Code hat der DHCP-Acknowledge?**

Op Code (op) = 2 (0x2)

**An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?**

CLIENT ADDRES: 255.255.255.255

**An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?**

FFFF.FFFF.FFFF 
Das ist die höchste MAC Adresse 

**Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?**

weil der Switch kann keine DAten speichern nur weiter schicken. 



**Welche IPv4-Adresse wird dem Client zugewiesen?**

192.168.27.35




### Discovers
![Discovers](./images/Discovers.png)

### Offers 
![Offers](./images/Offers.png)

### Request
![Request](./images/Request.png)

### Acknowledg 
![Acknowledg](./images/ACknowledg.png)